(function ($) {
/**
 * Attaches the Picker behovior to the page.
 */
Drupal.GooglePicker = {
  picker: {},
    views: {
      DOCS: 'DOCS',
      IMAGE_SEARCH: 'IMAGE_SEARCH',
      PHOTO_UPLOAD: 'PHOTO_UPLOAD',
      PHOTOS: 'PHOTOS',
      RECENTLY_PICKED: 'RECENTLY_PICKED',
      VIDEO_SEARCH: 'VIDEO_SEARCH',
    },
    createPicker: function() {
      pick = new google.picker.PickerBuilder();
      for(var key in this.views) {
        pick.addView(google.picker.ViewId[key]);
      }
      this.picker = pick.setCallback(this.pickerCallback)
        .build();
    },
    pickerCallback: function(data) {
      console.dir(data);
      if (data.action == 'cancel') {
        return false;
      }
      var message = Picker.display[data.docs[0].type](data);
      $('#result').text(message);
    },
    display: {
      video: function(data) {
          var message = 'VIDEO You picked: ' +
            ((data.action == google.picker.Action.PICKED) ? data.docs[0].url : 'nothing');
          return message;
      },
      photo: function(data) {
        var message = 'PHOTO You picked: ' +
          ((data.action == google.picker.Action.PICKED) ? data.docs[0].url : 'nothing');
        return message
      },
      document: function(data) {
        var message = 'DOCUMENT You picked: ' +
          ((data.action == google.picker.Action.PICKED) ? data.docs[0].url : 'nothing');
        return message;
      },
    },
    init: function() {
      this.createPicker();
    }
  };
})(jQuery);
